#!/usr/bin/env perl
use Mojolicious::Lite;
use MongoDB;
use Cwd;
use Time::Local;
use Data::Dumper;
use Encode qw(decode);
use List::Util qw/sum/;
use List::MoreUtils qw/zip/;
use DDP;

use constant {
    CMS => ['Нет', '1C-Bitrix', 'Drupal', 'WordPress', 'Joomla', 'MODx'],
    CALL => ['Нет', 'Callbackhunter', 'Perezvonim'],
    LANG => ['Неизвестно', 'Русский'],
    ASSISTANT => [ 
        'Нет',       'JivoSite',  'RedHelper', 'Livetex',
        'LiveChat',  'Regidium',  'Krible',    'Marva',
        'SiteHeart', 'Netrox SC', 'Chatra.io', 'Me-talk.ru'
    ],
};

# version 2.2
# --- Настройки ---
my $home = app->home();
my $config = plugin Config => { file => $home->rel_dir('web_view.conf') };

my $app = app;
$app->mode($config->{mode});
$app->static->paths->[0] = $home->to_string;
$app->secrets( ['!Very*Storong*Secret*Key*2017!'] );

# Логгирование
my $log = Mojo::Log->new(
   path  => $home->rel_dir('var/www/debug.log'),
   level => $config->{mode} eq 'development' ? 'debug' : 'info'
);
$app->log($log);
$SIG{__WARN__} = sub {
    $app->log->warn(@_);
};
$SIG{__DIE__} = sub {
    my $message = shift;
    return if $message =~ m/alarm/;
    $app->log->fatal($message);
};
# ---------

# plugin NYTProf => {
#   nytprof => {
#     ... # see CONFIGURATION
#   },
# };

# $log->warn("--- app static path: ".p($app->static->paths));

get '/' => sub {
    my $c = shift;

    if ($config->{mode} eq 'development' or $c->req->headers->host =~ /^titan\.hostx\.ru/i) {
        $c->stash(title => 'REGIDIUM CRM');
        $c->render( template => 'default' );
    # } else {
    #     $c->redirect_to('http://regidium.ru');
    }


    # if($c->req->headers->host =~ /^r.regidium.com/i) {
        # $c->stash(title => 'REGIDIUM CRM');
        # $c->render( template => 'default' );
    # } else {
    #     $c->redirect_to('http://regidium.ru');
    # }
};

get '/api/domains' => sub {
    my $c = shift;

    unless ( $c->session('login') ) {
        $c->res->headers->cache_control('max-age=1, no-cache');
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }

    my $mongo       = MongoDB::MongoClient->new();
    my $db          = $mongo->get_database('parser');
    my $col_domains = $db->get_collection('domains');

    # $log->debug('col domains: '.p($col_domains));

    my $domain              = $c->param('domain');
    my $cms                 = $c->param('cms');
    my $assistant           = $c->param('assistant');
    my $call                = $c->param('call');
    my $begin_reg_date      = $c->param('begin_reg_date');
    my $end_reg_date        = $c->param('end_reg_date');
    my $begin_request_time  = $c->param('begin_request_time');
    my $end_request_time    = $c->param('end_request_time');

    # $c->session(domain => $domain) if $domain;
    
    my %query = (
        status => 200,
        ($domain                ? (url       => lc($domain)) : ()),
        # ($c->session('domain')  ? (url       => lc($c->session('domain'))) : ()),
        (defined $cms           ? (cms       => (0 + $cms))                : ()),
        (defined $assistant     ? (assistant => (0 + $assistant))          : ()),
        (defined $call          ? (call      => (0 + $call))               : ()),
        (
           ($begin_reg_date or $end_reg_date) 
               ?  (
                       reg_date => {
                           ($begin_reg_date ? ('$gte' => convet_date_to_unixtime( $begin_reg_date )) : ()),
                           ($end_reg_date ? ('$lte' => convet_date_to_unixtime( $end_reg_date )) : ()),
                       }
                  ) 
               : ()
        ),
        (
           ($begin_request_time or $end_request_time) 
               ?  (
                       request_time => {
                           ($begin_request_time ? ('$gte' => ($begin_request_time+0)) : ()),
                           ($end_request_time ? ('$lte' => ($end_request_time+0)) : ()),
                       }
                  ) 
               : ()
        ),
    );

    # $log->debug("--- domains query ".Dumper(%query));

    my $domains_count = $col_domains->count( \%query );

    # $log->debug("--- domains count: $domains_count");

    my $total_page = int( $domains_count / 50 );
    $total_page++ if ( $domains_count % 50 ) > 0;

    my ($page_id, @pagination) = paggination_list(($c->param('page_id') || 0), $total_page, 15);

    # $log->debug("--- page_id: $page_id");
    # $log->debug('--- skip: '. ( ( ($page_id || 1) - 1 ) * 50 ));
    # $log->debug("--- domains query ".p(%query));

    # $log->debug(sprintf('db.domains.find(%s)', ));

    # TODO: заменить limit+skip на ranged query
    my $cursor = $col_domains
        ->find( \%query )
        ->limit(50)
        ->skip( ( ($page_id || 1) - 1 ) * 50 )
        ->sort({url => 1});

    # $log->debug("--- cursor: ".p($cursor));
    
    my @domains =
      map {
        $_->{reg_date}    = convet_date( $_->{reg_date} );
        $_->{last_update} = convet_date( $_->{last_update} );
        $_->{assistant}   = ASSISTANT->[$_->{assistant}];
        $_->{cms}         = CMS->[$_->{cms}];
        $_->{call}        = CALL->[$_->{call}];
        $_->{lang}        = LANG->[$_->{lang}];
        $_
      } $cursor->all;

    # $log->debug("--- domains: ".(scalar @domains));
      
    
    $c->res->headers->cache_control('public, max-age=3600');
    $c->render(
        json => {
            list       => \@domains,
            pagination => \@pagination,
            page_id    => $page_id,
            total_page => $total_page,
            count      => $domains_count,
            status     => 'ok'
        }
    );
};

get '/api/domain/#domain' => sub {
    my $c = shift;
    unless ( $c->session('login') ) {
        $c->res->headers->cache_control('max-age=1, no-cache');
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }
    my $mongo       = MongoDB::MongoClient->new();
    my $db          = $mongo->get_database('parser');
    my $col_domains = $db->get_collection('domains');
    my $col_domain_history = $db->get_collection('domain_history');
    my $domain      = $col_domains->find_one( { url => $c->stash('domain') } );
    my $domain_history = $col_domain_history->find_one( { url => $c->stash('domain') } );

    # $log->debug('domain: '.p($domain));
    if (    $domain->{lock}->{login}
        and $domain->{lock}->{login} ne $c->session('login')
        and $domain->{lock}->{ts} + 60 > time )
    {
        return $c->render(
            json => {
                status => 'error',
                code   => 423,
                user   => $domain->{lock}->{login}
            }
        );
    }

    $col_domains->update_one(
        { url    => $c->stash('domain') },
        { '$set'    => { lock => { login => $c->session('login'), ts => time } } },
    );

    # $col_domains->replace_one(
    #     { url    => $c->stash('domain') },
    #     { url    => $c->stash('domain'), lock => { login => $c->session('login'), ts => time } },
    # );

    $domain->{history} = [
        map {
            $_->{ts}        = convet_date( $_->{ts} );
            $_->{assistant} = ASSISTANT->[$_->{assistant}];
            $_->{cms}       = CMS->[$_->{cms}];
            $_->{call}      = CALL->[$_->{call}];
            $_
        } @{ $domain_history->{history} }
      ]
      if exists $domain_history->{history};

    $domain->{last_update}    = convet_date( $domain->{last_update} );
    $domain->{assistant}      = ASSISTANT->[$domain->{assistant}];
    $domain->{cms}            = CMS->[$domain->{cms}];
    $domain->{call}           = CALL->[$domain->{call}];
    $domain->{lang}           = LANG->[$domain->{lang}];

    $domain->{status_history} = [
        map {
            $_->{ts}     = convet_date( $_->{ts} );
            $_->{status} = convert_status( $_->{status} );
            $_
        } @{ $domain->{status_history} }
      ]
      if exists $domain->{status_history};
    $domain->{status_curent}->{date} =
      convet_date_time( $domain->{status_curent}->{date} )
      if exists $domain->{status_curent}->{date};
    $domain->{status} = 'ok';
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->render( json => $domain );
};

post '/api/domain/#domain' => sub {
    my $c = shift;
    unless ( $c->session('login') ) {
        $c->res->headers->cache_control('max-age=1, no-cache');
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }
    my $params = $c->req->json;
    $params->{date}  = convet_to_unixtime( $params->{date} ) if $params->{date};
    $params->{ts}    = time;
    $params->{login} = $c->session('login');
    my $mongo       = MongoDB::MongoClient->new();
    my $db          = $mongo->get_database('parser');
    my $col_domains = $db->get_collection('domains');
    my $col_users   = $db->get_collection('users');
    my $col_domain_history = $db->get_collection('domain_history');
    my $domain      = $col_domains->find_one( { url => $c->stash('domain') } );

    if (    $domain->{lock}->{login}
        and $domain->{lock}->{login} ne $c->session('login')
        and $domain->{lock}->{ts} + 60 > time )
    {
        return $c->render(
            json => {
                status => 'error',
                code   => 423,
                user   => $domain->{lock}->{login}
            }
        );
    }
    $col_domains->update_one(
        { url => $c->stash('domain') },
        {
            '$set' => {
                lock          => { login => $c->session('login'), ts => time },
                status_curent => $params
            },
            '$push' => {
                status_history => $params
            }
        }
    );
    $col_users->update_one(
        { login => $domain->{status_curent}->{login} },
        {
            '$pull' => {
                tasks => { url => $c->stash('domain') }
            }
        },
    );
    $params->{url} = $c->stash('domain');

    if ( $params->{status} == 1 or $params->{status} == 2 ) {
        $col_users->update_one(
            { login => $c->session('login') },
            {
                '$push' => {
                    tasks => $params
                }
            }
        );
    }

    my $domain_history = $col_domain_history->find_one( { url => $c->stash('domain') } );
    $domain = $col_domains->find_one( { url => $c->stash('domain') } );
    $domain->{history} = [
        map {
            $_->{ts}        = convet_date( $_->{ts} );
            $_->{assistant} = ASSISTANT->[$_->{assistant}];
            $_->{cms}       = CMS->[$_->{cms}];
            $_->{call}      = CALL->[$_->{call}];
            $_
        } @{ $domain_history->{history} }
      ]
      if exists $domain_history->{history};

    # $domain->{history} = [
    #     map {
    #         $_->{ts}        = convet_date( $_->{ts} );
    #         $_->{assistant} = ASSISTANT->[$_->{assistant}];
    #         $_->{cms}       = CMS->[$_->{cms}];
    #         $_->{call}      = CALL->[$_->{call}];
    #         $_
    #     } @{ $domain->{history} }
    #   ]
    #   if exists $domain->{history};

    $domain->{last_update}    = convet_date( $domain->{last_update} );
    $domain->{assistant}      = ASSISTANT->[$domain->{assistant}];
    $domain->{cms}            = CMS->[$domain->{cms}];
    $domain->{call}           = CALL->[$domain->{call}];
    $domain->{lang}           = LANG->[$domain->{lang}];

    $domain->{status_history} = [
        map {
            $_->{ts}     = convet_date( $_->{ts} );
            $_->{status} = convert_status( $_->{status} );
            $_
        } @{ $domain->{status_history} }
      ]
      if exists $domain->{status_history};

    $domain->{status_curent}->{date} =
      convet_date_time( $domain->{status_curent}->{date} )
      if exists $domain->{status_curent}->{date};

    $domain->{status} = 'ok';
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->render( json => $domain );
};

get '/api/errors' => sub {
    my $c = shift;
    unless ( $c->session('login') ) {
        $c->res->headers->cache_control('max-age=1, no-cache');
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }
    my $mongo       = MongoDB::MongoClient->new(query_timeout => 50000);
    my $db          = $mongo->get_database('parser');
    my $col_domains = $db->get_collection('domains');
    my @query;
    push @query, url => $c->param('domain') if ( $c->param('domain') );

    my $errors_count = $col_domains->count( { @query, status => { '$ne' => 200 } } );

    my $total_page = int( $errors_count / 50 );
    $total_page++ if ( $errors_count % 50 ) > 0;
    my $page_id = $c->param('page_id');
    my @pagination;
    ( $page_id, @pagination ) = paggination_list( $page_id, $total_page, '15' );
    my $cursor =
      $col_domains->find( { @query, status => { '$ne' => 200 } } )->limit(50)
      ->skip( ( $page_id - 1 ) * 50 );

    my @domains =
      map {
        $_->{last_update} = convet_date( $_->{last_update} );
        $_->{error_message} = decode( 'UTF8', $_->{error_message} );
        $_
      } $cursor->all;

    $c->res->headers->cache_control('public, max-age=259200');
    $c->render(
        json => {
            list       => \@domains,
            # list       => [@domains],
            pagination => \@pagination,
            # pagination => [@pagination],
            page_id    => $page_id,
            total_page => $total_page,
            count      => $errors_count,
            status     => 'ok'
        }
    );
};

get '/api/statistic' => sub {
    my $c = shift;
    unless ( $c->session('login') ) {
        $c->res->headers->cache_control('max-age=1, no-cache');
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }

    my $mongo = MongoDB::MongoClient->new();
    my $db    = $mongo->get_database('parser');

    my $begin_reg_date      = $c->param('begin_reg_date');
    my $end_reg_date        = $c->param('end_reg_date');

    $log->debug("reg_date: $begin_reg_date \n $end_reg_date \n") if $begin_reg_date && $end_reg_date;

    my %query = (
        (
           ($begin_reg_date or $end_reg_date) 
               ?  (
                       ts => {
                           ($begin_reg_date ? ('$gte' => convet_date_to_unixtime( $begin_reg_date )) : ()),
                           ($end_reg_date ? ('$lte' => convet_date_to_unixtime( $end_reg_date )) : ()),
                       }
                  ) 
               : ()
        ),
    );

    my $col_statistic = $db->get_collection('statistic');

    my $cursor = $col_statistic->find( \%query );

    my (
        @labels,    @drupal,      @bitrix,   @wordpress,
        @joomla,    @modx,        @livetex,  @jivosite,
        @redhelper, @livechatinc, @regidium, @krible,
        @marva,     @siteheart,   @netrox,   @chatra,
        @callbackhunter, @perezvonim,@metalk, @total_domains,
        @newdomains, @removed, @error_domains, @good_domains
    );

    for my $one_day ( $cursor->all ) {
        push @labels,         convet_date( $one_day->{ts} );
        push @drupal,         $one_day->{drupal} || 0;
        push @bitrix,         $one_day->{bitrix} || 0;
        push @wordpress,      $one_day->{wordpress} || 0;
        push @joomla,         $one_day->{joomla} || 0;
        push @modx,           $one_day->{modx} || 0;
        push @livetex,        $one_day->{livetex} || 0;
        push @jivosite,       $one_day->{jivosite} || 0;
        push @livechatinc,    $one_day->{livechatinc} || 0;
        push @regidium,       $one_day->{regidium} || 0;
        push @redhelper,      $one_day->{redhelper} || 0;
        push @krible,         $one_day->{krible} || 0;
        push @marva,          $one_day->{marva} || 0;
        push @siteheart,      $one_day->{siteheart} || 0;
        push @netrox,         $one_day->{netroxsc} || 0;
        push @chatra,         $one_day->{chatra} || 0;
        push @metalk,         $one_day->{metalk} || 0;
        push @callbackhunter, $one_day->{callbackhunter} || 0;
        push @perezvonim,     $one_day->{perezvoni} || 0;
        push @total_domains,  ($one_day->{total_domains} || 0)+0;
        push @newdomains,     $one_day->{newdomains} || 0;
        push @removed,        $one_day->{removed} || 0;
        push @error_domains,  $one_day->{errors} || 0;
        push @good_domains,   (($one_day->{total_domains} || 0) - ($one_day->{errors} || 0));
    }
    
    my $total_stat = $col_statistic->find({},{limit => 1, sort => {'$natural' => -1}});
    my (@callers,@assistants,@cmses);
    foreach my $line($total_stat->all) {
        #push @total_pie,  $line->{total_domains} || 0;
        
        push @callers,    
            ['Не установлено', ($line->{total_domains} - ($line->{perezvoni} + $line->{callbackhunter}))],
            ['Callbackhunter', $line->{callbackhunter} || 0],
            ['Perezvoni', $line->{perezvoni} || 0];

        push @cmses,      
            ['Не установлено', ($line->{total_domains} - (sum grep defined, @$line{qw/drupal bitrix wordpress joomla modx/}))], 
            ['CMS Drupal', $line->{drupal} || 0],
            ['CMS 1C-Bitrix', $line->{bitrix} || 0],
            ['CMS WordPress', $line->{wordpress} || 0],
            ['CMS Joomla', $line->{joomla} || 0],
            ['CMS MODx', $line->{modx} || 0];
        
        push @assistants, 
            ['Не установлено', ($line->{total_domains} 
                - (sum grep defined, @$line{qw/chatra jivosite krible livechatinc livetex marva metalk netroxsc redhelper regidium siteheart/} ))],
            ['Livetex', $line->{livetex} || 0],
            ['JivoSite', $line->{jivosite} || 0],
            ['LiveChat', $line->{livechatinc} || 0],
            ['Regidium', $line->{regidium} || 0],
            ['RedHelper', $line->{redhelper} || 0],
            ['Krible', $line->{krible} || 0],
            ['Marva', $line->{marva} || 0],
            ['SiteHeart', $line->{siteheart} || 0],
            ['Netrox SC', $line->{netroxsc} || 0],
            ['Chatra.io', $line->{chatra} || 0],
            ['Me-Talk.ru', $line->{metalk} || 0];
    }
     
    # TODO: добавиь тесты на структуру
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->render(
        json => {
            status => 'ok',
            cms    => {
                labels => \@labels,
                series => [
                    { name => "CMS Drupal",    data => \@drupal },
                    { name => "CMS 1C-Bitrix", data => \@bitrix },
                    { name => "CMS WordPress", data => \@wordpress },
                    { name => "CMS Joomla",    data => \@joomla },
                    { name => "CMS MODx",      data => \@modx }
                ]
            },
            assistant => {
                labels => \@labels,
                series => [
                    { name => "Livetex",   data => \@livetex },
                    { name => "JivoSite",  data => \@jivosite },
                    { name => "LiveChat",  data => \@livechatinc },
                    { name => "Regidium",  data => \@regidium },
                    { name => "RedHelper", data => \@redhelper },
                    { name => "Krible",    data => \@krible },
                    { name => "Marva",     data => \@marva },
                    { name => "SiteHeart", data => \@siteheart },
                    { name => "Netrox SC", data => \@netrox },
                    { name => "Chatra.io", data => \@chatra },
                    { name => "Me-Talk.ru", data => \@metalk }
                ]
            },
            call => {
                labels => \@labels,
                series =>
                  [ { name => "Callbackhunter", data => \@callbackhunter },
                    { name => "Perezvoni", data => \@perezvonim },]
            },
            domains => {
                labels => \@labels,
                series =>
                  [ 
                    { name => "New", data => \@newdomains },
                    { name => "Removed", data => \@removed },
                    ]
            },
            total_dom => {
              labels => \@labels,
                series =>
                  [ 
                    { name => "Total", data => \@total_domains },
                    ]  
            },
            total_stat => {
              labels => \@labels,
                series =>
                  [ 
                    { name => "Responded", data => \@good_domains },
                    { name => "Not Responded", data => \@error_domains },
                    ]  
            },
            #### pie charts
            total_call => {
                series => [ { name => "Callers", data => \@callers } ]  
            },
            
            total_cms => {
                series => [ { name => "CMS", data => \@cmses } ]
            },
            
            total_assist => {
                series => [ { name => "Assists", data => \@assistants }]
            }

        }
    );
};

post '/api/users/add' => sub {
    my $c = shift;
    unless ( $c->session('login') and ( $c->session('login') eq 'admin' ) ) {
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }
    my $mongo     = MongoDB::MongoClient->new();
    my $db        = $mongo->get_database('parser');
    my $col_users = $db->get_collection('users');
    $col_users->update_one(
        { login => $c->req->json->{login} },
        {
            '$set' => $c->req->json
        },
        { upsert => 1 }
    );
    my $cursor = $col_users->find();
    my @users =
      map {
        delete $_->{'_id'};
        delete $_->{'password'};
        delete $_->{'user'};
        $_
      } $cursor->all;
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->render( json => { list => [@users], status => 'ok' } );
};

get '/api/users/list' => sub {
    my $c = shift;
    unless ( $c->session('login') and ( $c->session('login') eq 'admin' ) ) {
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }
    my $mongo     = MongoDB::MongoClient->new();
    my $db        = $mongo->get_database('parser');
    my $col_users = $db->get_collection('users');
    my $cursor    = $col_users->find();
    my @users =
      map {
        delete $_->{'_id'};
        delete $_->{'password'};
        delete $_->{'user'};
        $_
      } $cursor->all;
    $c->res->headers->cache_control('max-age=1, no-cache');
    my $parser_status = get_status();
    $c->render( json =>
          { list => [@users], parser_status => $parser_status, status => 'ok' }
    );
};

get '/api/users/get' => sub {
    my $c = shift;
    unless ( $c->session('login') and ( $c->session('login') eq 'admin' ) ) {
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }
    my $mongo     = MongoDB::MongoClient->new();
    my $db        = $mongo->get_database('parser');
    my $col_users = $db->get_collection('users');
    my $user      = $col_users->find_one( { login => $c->param('login') },
        { login => 1, name => 1 } );

    $user->{status} = 'ok';
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->render( json => $user );
};

post '/api/users/remove' => sub {
    my $c = shift;
    unless ( $c->session('login') and ( $c->session('login') eq 'admin' ) ) {
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }
    my $mongo     = MongoDB::MongoClient->new();
    my $db        = $mongo->get_database('parser');
    my $col_users = $db->get_collection('users');
    $col_users->remove(
        {
            login => $c->req->json->{login}
        }
    );
    my $cursor = $col_users->find();
    my @users =
      map {
        delete $_->{'_id'};
        delete $_->{'password'};
        delete $_->{'user'};
        $_
      } $cursor->all;
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->render( json => { list => [@users], status => 'ok' } );
};

get '/api/users/tasks' => sub {
    my $c = shift;
    unless ( $c->session('login') ) {
        return $c->render(
            json => {
                status => 'error',
                code   => 403
            }
        );
    }
    my $mongo     = MongoDB::MongoClient->new();
    my $db        = $mongo->get_database('parser');
    my $col_users = $db->get_collection('users');
    my $user =
      $col_users->find_one( { login => $c->session('login') }, { tasks => 1 } );

    $user->{status} = 'ok';

    # $user->{tasks} =
    # [ map { $_->{status} = convert_task( $_->{status} ); $_ }
    # @{ $user->{tasks} } ];
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->render( json => $user );
};

post '/api/login' => sub {
    my $c         = shift;
    my $mongo     = MongoDB::MongoClient->new();
    my $db        = $mongo->get_database('parser');
    my $col_users = $db->get_collection('users');
    my $user      = $col_users->find_one(
        {
            login    => $c->req->json->{login},
            password => $c->req->json->{password}
        },
        { name => 1, login => 1, type => 1 }
    );
    if ( $user->{login} ) {
        $c->session(
            expiration => 604800,
            login      => $user->{login},
            name       => $user->{name},
            type       => $user->{type}
        );
        $c->res->headers->cache_control('max-age=1, no-cache');
        $c->render( json => { status => 'ok' } );
    }
    else {
        $c->res->headers->cache_control('max-age=1, no-cache');
        $c->render( json => { status => 'error' } );
    }

};

# ЛОГАУТ
get '/logout' => sub {
    my $c = shift;
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->res->headers->expires('Thu, 01 Dec 1994 16:00:00 GMT');
    $c->session(expires => 1);
    $c->redirect_to('/#/login');
};

app->start;

sub get_status () {
    my $mongo      = MongoDB::MongoClient->new();
    my $db         = $mongo->get_database('parser');
    my $col_status = $db->get_collection('status');
    my $status     = $col_status->find_one( { status => 'curent' } );

    if ( $status->{working} ) {
        my $working_time = time - $status->{start_time};
        my $hour         = int( $working_time / ( 60 * 60 ) );
        my $min          = ( $working_time / 60 ) % 60;
        my $sec          = $working_time % 60;
        return
            'Идет парсинг. Выполнено '
          . sprintf( "%.2f", $status->{done} ) . "% ( "
          . $status->{current_domains}
          . " из "
          . $status->{count_domains}
          . " ). Время работы парсера: "
          . sprintf( "%d:%02d:%02d", $hour, $min, $sec );
    }
    else {
        return '';
    }
}

sub paggination_list {
    my ( $page_id, $total_page, $pagination_max_pages ) = @_;
    my @pagination;
    if ( $page_id > $total_page ) {
        $page_id = $total_page;
    }
    elsif ( !$page_id or $page_id < 0 ) {
        $page_id = 1;
    }

    my $first_page = $page_id - int( $pagination_max_pages / 2 );
    if ( $first_page <= 1 ) {
        $first_page = 1;
    }
    else {
        if ( ( $total_page - $first_page ) < $pagination_max_pages ) {
            $first_page = $total_page - $pagination_max_pages + 1;
            if ( $first_page <= 1 ) { $first_page = 1; }
        }
    }
    my $last_page = $first_page + $pagination_max_pages - 1;
    if ( $last_page > $total_page ) { $last_page = $total_page; }
    if ( $first_page != 1 ) { push @pagination, '<'; }
    for ( my $i = $first_page ; $i <= $last_page ; $i++ ) {
        push @pagination, $i;
    }
    if ( $last_page < $total_page ) { push @pagination, '>'; }
    return $page_id, @pagination;
}

sub convet_date {
    my ($mday, $mon, $year) = (localtime($_[0]))[3, 4, 5];
    sprintf( "%02d.%02d.%04d", $mday, $mon + 1, $year + 1900 );
}

sub convet_date_time {
    my ( undef, $min, $hour, $mday, $mon, $year ) = localtime(shift);
    return sprintf(
        "%02d.%02d.%04d %02d:%02d",
        $mday, $mon + 1, $year + 1900,
        $hour, $min
    );
}

# sub convert_cms {
#     my $cms_id = shift;
#     my @cms = ( 'Нет', '1C-Bitrix', 'Drupal', 'WordPress', 'Joomla', 'MODx' );
#     return $cms[$cms_id];
# }

# sub convert_assistant {
#     my $assistant_id = shift;
#     my @assistant    = (
#         'Нет',    'JivoSite',  'RedHelper', 'Livetex',
#         'LiveChat',  'Regidium',  'Krible',    'Marva',
#         'SiteHeart', 'Netrox SC', 'Chatra.io', 'Me-talk.ru'
#     );
#     return $assistant[$assistant_id];
# }

# sub convert_call {
#     my $call_id = shift;
#     my @call = ( 'Нет', 'Callbackhunter', 'Perezvonim' );
#     return $call[$call_id];
# }

# sub convert_lang {
#     my $lang_id = shift;
#     my @lang = ( 'Неизвестно', 'Русский' );
#     return $lang[$lang_id];
# }

sub convert_status {
    my $status_id = shift;
    my @status    = (
        'Не задан',       'Перезвонить',
        'На контроле', 'Установлен',
        'Снят',              'Отказ'
    );
    return $status[$status_id];
}

sub convert_task {
    my $task_id = shift;
    my @task =
      ( undef, 'Установить связь', 'Связаться' );
    return $task[$task_id];
}

sub convet_to_unixtime {
    my $time = shift;
    $time =~ m/(\d\d)\.(\d\d)\.(\d{4}) (\d\d):(\d\d)/;
    return timelocal( 0, int($5), int($4), int($1), ( int($2) - 1 ), int($3) );
}

sub convet_date_to_unixtime {
    my $time = shift;
    if ( $time =~ m/(\d\d)\.(\d\d)\.(\d{4})/ ) {
        return timelocal( 12, 0, 0, $1, $2 - 1, $3 );
    }
    else {
        print "ERROR Time\n";
        return 0;
    }
}

__DATA__
@@ default.html.ep
<html ng-app="webviewApp">
<head>
<title><%= $title %></title>
  <meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link href="/css/dashboard.css" rel="stylesheet">
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#/">Domains WebView</a>
        </div>
      <ul class="nav navbar-nav">
      <li><a href="#/">Домены</a></li>
      <li><a href="#/statistic">Статистика</a></li>
      <li><a href="#/errors">Ошибки</a></li>
      <li><a href="#/tasks">Мои задачи</a></li>
      <li><a href="#/calendar">Календарь</a></li>
      </ul>
      
       <ul class="nav navbar-nav navbar-right">
         % if ( session('login') and ( session('login') eq 'admin' ) ) {
          <li><a href="#/admin">Пользователи</a></li>
          %}
        <li><a href="/logout">Выход</a></li>
       </ul>
    
  </nav>
<div ng-controller="notificationCenter"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-3 col-md-2 sidebar">
        <ul class="nav nav-sidebar">
          <li>
            <a href="#/">Домены</a>
          </li>
          <li>
            <a href="#/statistic">Статистика</a>
          </li>
          <li>
            <a href="#/errors">Ошибки</a>
          </li>
          <li>
            <a href="#/tasks">Мои задачи</a>
          </li>
          <li>
            <a href="#/calendar">Календарь</a>
          </li>
        </ul>
      </div>
      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div ng-view></div>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="/js/moment.js"></script>
  <script src="/js/angular/angular.min.js"></script>
  <script src="/js/angular/angular-route.min.js"></script>
<script src="/js/angular-ui-notification.min.js"></script>
  <script src="/js/app.js"></script>
  <script src="/js/controllers.js"></script>
  <script src="/js/directive.js"></script>
  <script src="/js/filters.js"></script>
<link rel="stylesheet" href="/css/angular-ui-notification.min.css">
  <link rel="stylesheet" href="/css/calendar.css">
  <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css" />
  <script type="text/javascript" src="/js/bootstrap-datetimepicker.min.js"></script>
  <script src="/js/fullcalendar.min.js"></script>
  <script src="/js/highcharts.js"></script>
  <script src="/js/highcharts-3d.js"></script>
</body>
</html>
