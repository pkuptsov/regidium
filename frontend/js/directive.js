var webviewDirectives = angular.module('webviewDirectives', []);


webviewDirectives.directive('accountname', function($q, $timeout) {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$asyncValidators.accountname = function(modelValue, viewValue) {
        var login_regexp = /^[a-z0-9_-]{3,10}$/i;
        if (ctrl.$isEmpty(modelValue)) {
          return $q.when();
        }

        var def = $q.defer();

        $timeout(function() {
          // Mock a delayed response
          if (login_regexp.test(viewValue)) {
            // The username is available
            def.resolve();
          } else {
            def.reject();
          }

        }, 1000);

        return def.promise;
      };
    }
  };
});

webviewDirectives.directive('username', function($q, $timeout) {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$asyncValidators.username = function(modelValue, viewValue) {
        var name_regexp = /^[a-zа-я0-9_ -]{3,30}$/i;
        if (ctrl.$isEmpty(modelValue)) {
          return $q.when();
        }

        var def = $q.defer();

        $timeout(function() {
          if (name_regexp.test(viewValue)) {
            def.resolve();
          } else {
            def.reject();
          }

        }, 1000);

        return def.promise;
      };
    }
  };
});

webviewDirectives.directive('password', function($q, $timeout) {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$asyncValidators.password = function(modelValue, viewValue) {
        var password_regexp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;

        if (ctrl.$isEmpty(modelValue)) {
          return $q.when();
        }
        var def = $q.defer();
        $timeout(function() {
          if (password_regexp.test(viewValue)) {
            def.resolve();
          } else {
            def.reject();
          }

        }, 1000);
        return def.promise;
      };
    }
  };
});
