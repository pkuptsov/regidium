var webviewApp = angular.module('webviewApp', [
  'ngRoute',
  'webviewControllers',
  'webviewDirectives',
  'webviewFilters'
]);

webviewApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: '/templates/domains.html',
        controller: 'DomainsListCtrl'
      }).
      when('/errors', {
        templateUrl: '/templates/errors.html',
        controller: 'ErrorsListCtrl'
      }).
      when('/domain/:domain', {
        templateUrl: '/templates/domainshow.html',
        controller: 'DomainShowCtrl'
      }).
      when('/statistic', {
        templateUrl: '/templates/statistic.html',
        controller: 'StatisticCtrl'
      }).
      when('/admin', {
        templateUrl: '/templates/users.html',
        controller: 'UsersListCtrl'
      }).
      when('/tasks', {
        templateUrl: '/templates/tasks.html',
        controller: 'TaskListCtrl'
      }).
      when('/calendar', {
        templateUrl: '/templates/calendar.html',
        controller: 'CalendarCtrl'
      }).
      when('/login', {
        templateUrl: '/templates/login.html',
        controller: 'UserLogin'
      }).
      otherwise({
        redirectTo: '/'
      });
  }]);
