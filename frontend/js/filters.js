var webviewFilters = angular.module('webviewFilters', []);


webviewFilters.filter('taskFilter', function() {
  return function(input, params) {
    var filtred = [],
      date;
    var beginDate = $('#dp7').val();
    if (beginDate) {
      date = beginDate.split('.');
      beginDate = new Date(date[2], date[1] - 1, date[0], 0, 0).getTime() / 1000;
    }
    var endDate = $('#dp8').val();
    if (endDate) {
      date = endDate.split('.');
      endDate = new Date(date[2], date[1] - 1, date[0], 23, 59).getTime() / 1000;
    }
    var now = new Date().getTime() / 1000;
    filtred = input.filter(function(task) {
      if (params[0] == task.status && task.date >= beginDate && task.date <= endDate) {
        if (params[0] === '1' && params[1] === '1') {
          if (task.date <= now) {
            return task;
          }
        } else {
          return task;
        }
      }
    });
    return filtred;
  };
});
