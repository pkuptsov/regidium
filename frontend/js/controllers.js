var webviewControllers = angular.module('webviewControllers', ['ui-notification']);

var errorsMsg = {
  connection: 'Ошибка подключения к серверу. Попробуйте повторить попытку позже.',
  login: 'Не верное имя пользователя или пароль.',
  busy: 'С этим домен уже работает пользователь '
};

function clone(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}

webviewControllers.controller('DomainsListCtrl', ['$scope', '$http', '$location',
  function($scope, $http, $location) {
    $http.get('/api/domains', {
      'params': {
        'page_id': 1
      }
    }).success(function(data) {
      var params = {
        'page_id': 1
      };

      if (data.status === 'ok') {
        $scope.domains = data;
      } else {
        if (data.code === 403) {
          $location.path('/login');
        }
      }
      $scope.getPage = function(pageID) {
        $scope.error = null;
        $scope.domains = null;
        params.page_id = pageID;
        $http.get('/api/domains', {
          'params': params
        }).success(function(data) {
          if (data.status === 'ok') {
            $scope.domains = data;
          } else {
            if (data.code === 403) {
              $location.path('/login');
            }
          }
        }).
        error(function() {
          $scope.error = errorsMsg.connection;
        });
      };
      $scope.serFilter = function(filters) {
        $scope.error = null;
        $scope.domains = null;
        if (filters) {
          params = filters;
        }

        if ($('#inputBeginDate').val()) {
          params.begin_reg_date = $('#inputBeginDate').val();
        }
        if ($('#inputEndDate').val()) {
          params.end_reg_date = $('#inputEndDate').val();
        }

        // TODO: при очистке поля в переменной (begin|end)_request_time остаются прошлые значения
        // они сбрасываются, только если указать 0 или другое значение
        // проверить: ввести 1 в поле "Время ответа от", нажать задать фильтр
        // затем очистить поле "от", ввести 1 в поле до, в параметре begin_request_time будет лежать прошлое значение 1
        // если явно 0 указать, то норм
        if ($('#inputRequestTimeBegin').val()) {
          params.begin_request_time = $('#inputRequestTimeBegin').val();
        }
        if ($('#inputRequestTimeEnd').val()) {
          params.end_request_time = $('#inputRequestTimeEnd').val();
        }
        $http.get('/api/domains', {
          'params': params
        }).success(function(data) {
          if (data.status === 'ok') {
            $scope.domains = data;
          } else {
            if (data.code === 403) {
              $location.path('/login');
            }
          }
        }).
        error(function() {
          $scope.error = errorsMsg.connection;
        });
      };
    }).
    error(function() {
      $scope.error = errorsMsg.connection;
    });
  }
]);

webviewControllers.controller('ErrorsListCtrl', ['$scope', '$http', '$location',
  function($scope, $http, $location) {
    $http.get('/api/errors', {
      'params': {
        'page_id': 1
      }
    }).success(function(data) {
      var params = {
        'page_id': 1
      };

      if (data.status === 'ok') {
        $scope.domains = data;
      } else {
        if (data.code === 403) {
          $location.path('/login');
        }
      }
      $scope.getPage = function(pageID) {
        $scope.error = null;
        $scope.domains = null;
        params.page_id = pageID;
        $http.get('/api/errors', {
          'params': params
        }).success(function(data) {
          if (data.status === 'ok') {
            $scope.domains = data;
          } else {
            if (data.code === 403) {
              $location.path('/login');
            }
          }
        }).
        error(function() {
          $scope.error = errorsMsg.connection;
        });
      };
      $scope.serFilter = function(filters) {
        $scope.error = null;
        $scope.domains = null;
        params = filters;
        $http.get('/api/errors', {
          'params': params
        }).success(function(data) {
          if (data.status === 'ok') {
            $scope.domains = data;
          } else {
            if (data.code === 403) {
              $location.path('/login');
            }
          }
        }).
        error(function() {
          $scope.error = errorsMsg.connection;
        });
      };
    }).
    error(function() {
      $scope.error = errorsMsg.connection;
    });
  }
]);

webviewControllers.controller('UsersListCtrl', ['$scope', '$http', '$location',
  function($scope, $http, $location) {
    var zeroUser = $scope.user;
    $http.get('/api/users/list').success(function(data) {
      if (data.status === 'ok') {
        $scope.data = data;
        if (data.parser_status.length === 0) {
          delete $scope.data.parser_status;
        }
      } else {
        if (data.code === 403) {
          $location.path('/login');
        }
      }
    }).
    error(function() {
      $scope.error = errorsMsg.connection;
    });
    $scope.save = function(user) {
      $scope.error = null;
      $http.post('/api/users/add', {
        'login': user.login,
        'password': user.password,
        'name': user.name
      }).
      success(function(data) {
        $scope.user = zeroUser;
        if (data.status === 'ok') {
          $scope.data = data;
          if (data.parser_status.length === 0) {
            delete $scope.data.parser_status;
          }
        } else {
          if (data.code === 403) {
            $location.path('/login');
          }
        }
      }).
      error(function() {
        $scope.error = errorsMsg.connection;
      });
    };
    $scope.remove = function(login) {
      $scope.error = null;
      $http.post('/api/users/remove', {
        'login': login
      }).
      success(function(data) {
        if (data.status === 'ok') {
          $scope.data = data;
          if (data.parser_status.length === 0) {
            delete $scope.data.parser_status;
          }
        } else {
          if (data.code === 403) {
            $location.path('/login');
          }
        }
      }).
      error(function() {
        $scope.error = errorsMsg.connection;
      });
    };
    $scope.edit = function(login) {
      $scope.user = zeroUser;
      $scope.data.list.forEach(function(user) {
        if (user.login === login) {
          $scope.user = user;
        }
      });
    };
  }
]);

webviewControllers.controller('StatisticCtrl', ['$scope', '$http',
  function($scope, $http) {
    $http.get('/api/statistic').success(function(data) {
      if (data.status === 'ok') {
        
      Highcharts.chart('total_stat', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Динамика изменения'
        },
        
        xAxis: {
           categories: data.total_stat.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во сайтов (ед.)'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.total_stat.series
    });
        
       Highcharts.chart('assistant', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Динамика изменения'
        },
        
        xAxis: {
           categories: data.assistant.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во установок (ед.)'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.assistant.series
    });
     
       
   Highcharts.chart('cms', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Динамика изменения'
        },
        
        xAxis: {
           categories: data.cms.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во установок (ед.)'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.cms.series
    });
     

     Highcharts.chart('call', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Динамика изменения'
        },
        
        xAxis: {
           categories: data.call.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во установок (ед.)'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.call.series
    });
     
      Highcharts.chart('domains', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Новые / Удаленные'
        },
        
        xAxis: {
           categories: data.domains.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.domains.series
    });
      
     Highcharts.chart('total_dom', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Общее количество'
        },
        
        xAxis: {
           categories: data.total_dom.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во '
            },
            min: 0
        },
        
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.total_dom.series
    });
     
    // pie
     Highcharts.chart('assistant_pie', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'Общее количество'
        },
        tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.3f}% ( {point.y:.0f} ) </b>'
            },        
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        
        series: data.total_assist.series
               
    });
     
     Highcharts.chart('cms_pie', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'Общее количество'
        },
        
        tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.3f}% ( {point.y:.0f} ) </b>'
            },
            
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        
        series: data.total_cms.series
               
    });
     
     Highcharts.chart('call_pie', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        
        title: {
            text: 'Общее количество'
        },
        
        tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.3f}% ( {point.y:.0f} ) </b>'
            },
            
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        
        series: data.total_call.series
               
    });   
     
       
      } else {
        if (data.code === 403) {
          $location.path('/login');
        }
      }
    }).
    error(function() {
      $scope.error = errorsMsg.connection;
    });
    $scope.serFilter = function() {
      $scope.error = null;
      var filters = {};
      filters.begin_reg_date = $('#inputBeginDateStat').val();
      filters.end_reg_date = $('#inputEndDateStat').val();
      params = filters;
      $http.get('/api/statistic', {
        'params': params
      }).success(function(data) {
        if (data.status === 'ok') {
        
              Highcharts.chart('assistant', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Динамика изменения'
        },
        
        xAxis: {
           categories: data.assistant.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во установок (ед.)'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.assistant.series
    });
     
       
   Highcharts.chart('cms', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Динамика изменения'
        },
        
        xAxis: {
           categories: data.cms.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во установок (ед.)'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.cms.series
    });
     

     Highcharts.chart('call', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Динамика изменения'
        },
        
        xAxis: {
           categories: data.call.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во установок (ед.)'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.call.series
    });
     
      Highcharts.chart('domains', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Новые / Удаленные'
        },
        
        xAxis: {
           categories: data.domains.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во'
            },
            min: 0
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.domains.series
    });
      
     Highcharts.chart('total_dom', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Общее количество'
        },
        
        xAxis: {
           categories: data.total_dom.labels
        },
        yAxis: {
            title: {
                text: 'Кол-во '
            },
            min: 0
        },
        
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data.total_dom.series
    });
     
    // pie
     Highcharts.chart('assistant_pie', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'Общее количество'
        },
        tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.3f}% ( {point.y:.0f} ) </b>'
            },        
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        
        series: data.total_assist.series
               
    });
     
     Highcharts.chart('cms_pie', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'Общее количество'
        },
        
        tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.3f}% ( {point.y:.0f} ) </b>'
            },
            
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        
        series: data.total_cms.series
               
    });
     
     Highcharts.chart('call_pie', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        
        title: {
            text: 'Общее количество'
        },
        
        tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.3f}% ( {point.y:.0f} ) </b>'
            },
            
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        
        series: data.total_call.series
               
    });   
        
        } else {
          if (data.code === 403) {
            $location.path('/login');
          }
        }
      }).
      error(function() {
        $scope.error = errorsMsg.connection;
      });
    };
  }
]);

webviewControllers.controller('DomainShowCtrl', ['$scope', '$routeParams', '$http', '$location', 'Notification',
  function($scope, $routeParams, $http, $location, Notification) {
    var nullTask = $scope.task;
    $http.get('/api/domain/' + $routeParams.domain).success(function(data) {
      if (data.status === 'ok') {
        $scope.domain = data;
        $scope.showHistory = false;
        $scope.historyControl = 'Подробнее';
        console.log(data);
        $scope.task = clone(data.status_curent);
      } else {
        if (data.code === 403) {
          $location.path('/login');
        } else if (data.code === 423) {
          $scope.error = errorsMsg.busy + data.login;
        }
      }
    }).
    error(function() {
      $scope.error = errorsMsg.connection;
    });
    $scope.change = function(task) {
      $scope.error = null;
      task.date = $('#taskDate').val();
      task.url =  $routeParams.domain;
      var timeToTask = moment(task.date, "DD.MM.YYYY HH:mm").format('x') - new Date().getTime();
      if (timeToTask > 0){
        setTimeout(showNotification, timeToTask, Notification, task);
      }
      console.log(task.date);
      $http.post('/api/domain/' + $routeParams.domain, task).
      success(function(data) {
        if (data.status === 'ok') {
          $scope.task = nullTask;
          $scope.domain = data;
          $scope.task = clone(data.status_curent);
        } else {
          if (data.code === 403) {
            $location.path('/login');
          } else if (data.code === 423) {
            $scope.error = errorsMsg.busy + data.login;
          }
        }
      }).
      error(function() {
        $scope.error = errorsMsg.connection;
      });
    };
    $scope.togleHistory = function() {
      $scope.showHistory = !$scope.showHistory;
      if ($scope.showHistory) {
        $scope.historyControl = 'Скрыть';
      } else {
        $scope.historyControl = 'Подробнее';
      }
    };
  }
]);

webviewControllers.controller('TaskListCtrl', ['$scope', '$http', '$location',
  function($scope, $http, $location) {
    $http.get('/api/users/tasks').success(function(data) {
      if (data.status === 'ok') {
        if (data.tasks) {
          data.tasks.sort(function(a, b) {
            return a.date - b.date;
          });
        }
        var today = new Date();
        $scope.task = {
          status: 1,
          overtime: 0
        };
        $('#dp7').val(moment(today).format("DD.MM.YYYY"));
        $('#dp8').val(moment(today).format("DD.MM.YYYY"));
        if (data.tasks) {
          $scope.tasks = data.tasks.map(function(task) {
            var toDay = new Date();
            toDay.setHours(0, 0, 0, 0);
            var taskDate = new Date(task.date * 1000);
            taskDate.setHours(0, 0, 0, 0);
            if (toDay < taskDate) {
              task['class'] = '';
            } else if (toDay > taskDate) {
              task['class'] = 'list-group-item-danger';
            } else {
              task['class'] = 'list-group-item-warning';
            }
            task.dateView = moment(task.date, "X").format("HH:mm DD.MM");
            return task;
          });
        } else {
          $scope.tasks = [];
        }
      } else {
        if (data.code === 403) {
          $location.path('/login');
        }
      }
    }).
    error(function() {
      $scope.error = errorsMsg.connection;
    });
  }
]);

webviewControllers.controller('CalendarCtrl', ['$scope', '$http', '$location',
  function($scope, $http) {
    $http.get('/api/users/tasks').success(function(data) {
      if (data.status === 'ok') {
        var Events = [];
        if (data.tasks) {
          Events = data.tasks.map(function(task) {
            var Event = {};
            Event.start = new Date(task.date * 1000);
            Event.title = task.url;
            Event.allDay = true;
            Event.url = '#/domain/' + task.url;
            return Event;
          });
        }

        $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: ''
          },
          editable: true,
          lang: 'ru',
          events: Events
        });
      } else {
        if (data.code === 403) {
          $location.path('/login');
        }
      }
    }).
    error(function() {
      $scope.error = errorsMsg.connection;
    });
  }
]);

webviewControllers.controller('UserLogin', ['$scope', '$http', '$location',
  function($scope, $http, $location) {
    $scope.login = function(user) {
      $scope.error = null;
      $http.post('/api/login', {
        'login': user.login,
        'password': user.password
      }).
      success(function(data) {
        if (data.status === "ok") {
          $location.path('/');
        } else {
          $scope.error = errorsMsg.login;
        }
      }).
      error(function() {
        $scope.error = errorsMsg.connection;
      });
    };
  }
]);

webviewControllers.controller('notificationCenter', ['$scope', '$http', '$location', 'Notification',
  function($scope, $http, $location, Notification){
  $http.get('/api/users/tasks').success(function(data) {
      if (data.status === 'ok') {
        var now = new Date().getTime();
        data.tasks.forEach(function (task){
          var timeToTask = task.date*1000 - now;
          if (timeToTask > 0){
            setTimeout(showNotification, timeToTask, Notification, task);
          }
        }
        );
      } else {
        if (data.code === 403) {
          $location.path('/login');
        }
      }
    });
  // ;
}]);

function showNotification (Notification, task) {
  var dateFormat = /^\d+\.\d+\.\d+ \d+:\d+$/;
  if(dateFormat.test(task.date)){
    task.date = moment(task.date, "DD.MM.YYYY HH:mm").format('X');
  }
  Notification.warning({ message: 'Контакт: '+task.name+'<br>Телефон: '+task.phone+'<br>Email: '+task.email+'<br>Время звонка: '+ moment(task.date, "X").format("HH:mm DD.MM"),
                         title: '<a href="#/domain/'+task.url+'">'+task.url+'</a>',
                         delay: null
                        });
  var audio = new Audio('/sound/notification.mp3');
  audio.play();
}

