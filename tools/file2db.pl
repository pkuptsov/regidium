#!/usr/bin/perl -w
use common::sense;
use MongoDB;
use Mojo::Log;
my $log = Mojo::Log->new(path => 'index_mojop.log', level => 'warn');
use constant DEBUG => 0;
# коннект к БД
my $mongo         = MongoDB::MongoClient->new(query_timeout => 10000);
my $db            = $mongo->get_database('parser2');
my $file = 'ru_domains';
# только статистика
my $nic = $db->get_collection('nicbase');
# $nic->drop; # clean before insert
#unless ($dev_mode) {
#    system(
#'wget --no-check-certificate --output-document=ru_domains.gz "https://www.nic.ru/downloads/ru_domains.gz?login=3193/NIC-REG&password=pro222A"'
#    );
#    system("gzip -fd  $file.gz");
#}
open(FILE, "<",$file) or die $!;
print "Starting process with file..\n";
while (<FILE>) {
my ( $url, $reg, $reg_date, $pay_date, $end_date, $status ) = split(/\s+/);
    $url = lc($url);
    next unless $url;
my $res = $nic->insert_one(
                 {
                    url => $url,
                    reg_date => $reg_date,
                    status => $status
                 }
                 );
print $res->inserted_id . "..";
}
close(FILE);
print "Now create the Index on url filed\n";
$nic->ensure_index({url => 1});
