#!/usr/bin/perl -w
use strict;
use warnings;

while(1) {
    chomp(my $con = `ss | grep http |wc -l`);
    $con =~ s/[^0-9]+//g;
    if ($con >= 1000) {
        print "\a$con\n";
    } else {
        print "$con..";
    }
    sleep 1;
}
