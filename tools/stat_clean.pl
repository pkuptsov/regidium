#!/usr/bin/perl -w
use common::sense;
use MongoDB;
use Mojo::Log;
my $log = Mojo::Log->new(path => 'app_mojop.log', level => 'warn');
use constant DEBUG => 1;
my $max = 20;
# коннект к БД
my $mongo         = MongoDB::MongoClient->new();
my $db            = $mongo->get_database('parser');
# только статистика
my $col_statistic = $db->get_collection('statistic');
# запрашиваем все записи с джумлой меньше $max 
my $joomla = $col_statistic->find({'joomla' => ''});#{ '$gt' => $max }});
# теперь все это вынимаем из статы
print "Processing.....\n";
while (my $ts = $joomla->next) {
        print $ts->{'ts'} . " : " . $ts->{'joomla'},"\n" if DEBUG;
        print "." unless DEBUG;
        $col_statistic->remove({ts => $ts->{'ts'}}) unless DEBUG;
    }
# finished
print "\nDone\n";
