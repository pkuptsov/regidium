#!/usr/bin/env perl

use strict;
use Mojo::UserAgent;
use Time::Local;
use MongoDB;
use threads;
use Thread::Queue;

my $production = 0;
my $tread_count = 128;
my $file = 'ru_domains_cut';
# в продакшене только
if($production) {
system(
'wget --no-check-certificate --output-document=/opt/parser/ru_domains.gz "https://www.nic.ru/downloads/ru_domains.gz?login=3193/NIC-REG&password=passwd"'
);
}

system("gzip -fd  $file.gz");

# предлагаю заменить на $count_all = `wc -l < $file`;
open( FILE, '<', '/opt/parser/ru_domains' );
my $count_all;
while (<FILE>) { $count_all++; }
close(FILE);

my $start_time = time;
my $Queue      = Thread::Queue->new();
my @threads;
push @threads, threads->create( \&QueueWorker );

foreach my $i ( 1 .. $tread_count ) {
    push @threads, threads->create( \&ParserWorker );
}

foreach my $thread (@threads) {
    $thread->join();
}

sub ParserWorker {
    sleep 5;
    my $mongo         = MongoDB::MongoClient->new();
    my $db            = $mongo->get_database('parser');
    my $col_domains   = $db->get_collection('domains');
    my $col_statistic = $db->get_collection('statistic');
    my $ua            = Mojo::UserAgent->new();
    # опции UA
    $ua->max_redirects(3)->connect_timeout(60)->request_timeout(30)
      ->inactivity_timeout(40);
    $ua->transactor->name(
'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
    );
    # колбэк и реферер
    $ua->on(
        start => sub {
            my ( $ua, $tx ) = @_;
            $tx->req->headers->header( 'Referer' => 'http://regidium.ru/' );
        }
    );
    
    while ( my $host = $Queue->extract() ) {# получаем из очереди имя домена
        my $url = Mojo::URL->new( 'http://' . $host );# формируем запрос
        my $tx  = $ua->get($url); # выполняем запрос
        if ( $tx->success ) {# если успешно - то парсим
            parse_html( $url, $tx, $col_domains, $col_statistic );
        }
        else {
            my $err = $tx->error;
            error_get_page( $url, $err, $col_domains );
        }
    }
}

sub QueueWorker {
    my $counter;
    my $mongo         = MongoDB::MongoClient->new();
    my $db            = $mongo->get_database('parser');
    my $col_domains   = $db->get_collection('domains');
    my $col_status    = $db->get_collection('status');
    my $col_statistic = $db->get_collection('statistic');
    $col_statistic->update(
        { ts => $start_time },
        {
            '$set' => { ts => $start_time }
        },
        { upsert => 1 }
    );
    open( DOMAINS, '<', '/opt/parser/ru_domains' );

    while (<DOMAINS>) {
        $counter++;
        my $ItemsInQueue = $Queue->pending();# получаем кол-во элементов в очереди
        my ( $url, $reg, $reg_date, $pay_date, $end_date, $status ) =
          split(/\s+/);
        $url = lc($url);
        if ( $status and $url ) {# если статус делегирован и есть url - добавляем в очередь
            $reg_date = convert_date($reg_date);
            $Queue->enqueue($url);# добавляем в очередь
            # вносим обновления в БД в таблицу доменов, ищем по урл и обновляем урл и дату регичтрации, 
            $col_domains->update(
                { url => $url },
                {
                    '$set' => {
                        url      => $url,
                        reg_date => $reg_date
                      }

                },
                { upsert => 1 }
            );
        }
        else {# либо удаляем из БД этот домен
            $col_domains->remove(
                {
                    url => $url
                }
            );
        }

        if ( $ItemsInQueue > 500 ) {# когда в очередь поставили 500 задач 
            my $done = $counter / $count_all * 100;# считаем процент выполнения от общего кол-ва доменов
            eval {
                $col_status->update(# обвноляем статус в БД
                    { status => 'curent' },
                    {
                        '$set' => {
                            status          => 'curent',
                            start_time      => $start_time,
                            count_domains   => $count_all,
                            current_domains => $counter,
                            working         => 1,
                            done            => $done
                        }
                    },
                    { upsert => 1 }
                );
            };
            
            my $i = 0;
             WAIT: while ( $ItemsInQueue > 500 ) {# здесь зачем-то засыпаем на 50 секунд
                sleep 5;
                last WAIT if (++$i>10);# выходим из цикла либо по истечении 50 секунд, либо
                $ItemsInQueue = $Queue->pending();# кода в очереди будет больше 500 заданий
            }
        }
        
    }
    close(DOMAINS);
    
    # обвновляем статус о том что очередь сделана на 500 заданий и ...
    eval {
        $col_status->update(
            { status => 'curent' },
            {
                '$set' => {
                    status    => 'curent',
                    stop_time => time,
                    working   => 0,
                }
            },
            { upsert => 1 }
        );
    };
    # засыпаем на 2 часа. Т.е. 500 заданий обрабатываются ~2 часа
    sleep 7200;
    exit 0;
}

sub parse_html {
    my ( $url, $tx, $col_domains, $col_statistic ) = @_;
    my $body      = $tx->res->body;
    my $assistant = 0;
    my $cms       = 0;
    my $call      = 0;
    my $lang      = 0;
    print $url->host . " success!\n";
    if ( $body =~ m!//code\.jivosite\.com/script/widget/! ) {
        $assistant = 1;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { jivosite => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!web\.redhelper\.ru/service/! ) {
        $assistant = 2;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { redhelper => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!livetex\.ru/js/client\.js! ) {
        $assistant = 3;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { livetex => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!cdn\.livechatinc\.com! ) {
        $assistant = 4;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { livechatinc => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!widget\.regidium\.com/widget\.js! ) {
        $assistant = 5;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { regidium => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!//cdn\.krible\.com/loader! ) {
        $assistant = 6;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { krible => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!account\.marva\.ru! ) {
        $assistant = 7;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { marva => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!widget\.siteheart\.com! ) {
        $assistant = 8;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { siteheart => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!//code\.netroxsc\.ru! ) {
        $assistant = 9;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { netroxsc => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!chat\.chatra\.io/chatra\.js! ) {
        $assistant = 10;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { chatra => 1 }
                }
            );
        };
    }
    if ( $body =~ m!/bitrix/templates/! ) {
        $cms = 1;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { bitrix => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!/sites/all/! ) {
        $cms = 2;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { drupal => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!wp-content/themes/! ) {
        $cms = 3;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { wordpress => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!media/system/! ) {
        $cms = 4;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { joomla => 1 }
                }
            );
        };
    }
    elsif ( $body =~ m!assets/! ) {
        $cms = 5;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { modx => 1 }
                }
            );
        };
    }

    if ( $body =~ m!//callbackhunter\.com/widget/tracker\.js! ) {
        $call = 1;
        eval {
            $col_statistic->update(
                { ts => $start_time },
                {
                    '$inc' => { callbackhunter => 1 }
                }
            );
        };
    }

    $lang = 1 if $body =~ m![а-я]!;
    eval {
        $col_domains->update(
            { url => $url->host },
            {
                '$set' => {
                    url           => $url->host,
                    cms           => $cms,
                    assistant     => $assistant,
                    call          => $call,
                    lang          => $lang,
                    status        => 200,
                    error_message => '',
                    last_update   => $start_time
                },
                '$push' => {
                    history => {
                        ts        => $start_time,
                        cms       => $cms,
                        assistant => $assistant,
                        call      => $call
                    }
                }
            }
        );
    };
    return;
}

sub error_get_page {
    my ( $url, $err, $col_domains ) = @_;
    print $url->host . " error!\n";
    eval {
        $col_domains->update(
            { url => $url->host },
            {
                '$set' => {
                    url           => $url->host,
                    status        => $err->{code},
                    error_message => $err->{message},
                    last_update   => $start_time
                }
            }
        );
    };
}

sub convert_date {
    my $time = shift;
    if ( $time =~ m/(\d\d)\.(\d\d)\.(\d{4})/ ) {
        return timelocal( 12, 0, 0, $1, $2 - 1, $3 );
    }
    else {
        print "ERROR Time\n";
        return 0;
    }
}

