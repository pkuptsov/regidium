#!/usr/bin/perl -w
use warnings;
use strict;
use feature 'say';

use constant DEBUG => 0;

use MongoDB;

# коннект к БД
my $mongo       = MongoDB::MongoClient->new(query_timeout => 180_000_000);
my $db          = $mongo->get_database('parser');
my $col_domain_history = $db->get_collection('domain_history');

# выборка документов с размером массива = 5
my $hist = $col_domain_history->find( { history => { '$size' => 6 } } );

my $cnt = 0;
# чистим БД в части history
while ( my $doc = $hist->next ) {
    say ((++$cnt).' '.$doc->{url}) if DEBUG;
    $col_domain_history->update_one(
        { url => $doc->{url} },
        {
            '$pop' => { history => -1 }
        },
    );
}

