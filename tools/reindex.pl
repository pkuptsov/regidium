#!/usr/bin/perl -w
use common::sense;
use MongoDB;
use Mojo::Log;
my $log = Mojo::Log->new(path => 'index_mojop.log', level => 'warn');
use constant DEBUG => 0;
# коннект к БД
my $mongo         = MongoDB::MongoClient->new(query_timeout => 180000000);
my $db            = $mongo->get_database('parser');
# только статистика
my $domains = $db->get_collection('domains');

my @indexes = qw();

#$domains->drop_index('foo_1');
# status 
print "Go...";
$domains->ensure_index( { status => 1, cms => 1, url => 1 });#,{background => 1} );
$domains->ensure_index( { status => 1, assistant => 1,  url => 1 });#,{background => 1} );
$domains->ensure_index( { status => 1, call => 1, url => 1 });#,{background => 1} );
$domains->ensure_index( { status => 1, cms => 1, assistant => 1, url => 1 });#,{background => 1} );
$domains->ensure_index( { status => 1, cms => 1, assistant => 1 , call => 1, url => 1});#,{background => 1} );
$domains->ensure_index( { status => 1, url => 1, reg_date => 1 } );

print "Done!";

