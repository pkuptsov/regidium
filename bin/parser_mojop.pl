#!/usr/bin/perl -w

use common::sense;
# use utf8;
# use open qw/:std :utf8/;

use AnyEvent;
use EV;
use AnyEvent::HTTP;
use AnyEvent::Log;

use MongoDB;
use Time::Local;
use Mojo::Log;
# use Time::HiRes qw/time/;
use List::MoreUtils qw/zip/;
use Scalar::Util qw/dualvar/;

use DDP;
use Data::Dumper;

use FindBin qw/$Bin/;
# use lib "$Bin/lib";
use lib "$Bin/../lib";
# TODO: Parser::Util -> Regidium::Parser::Util
use Titan::Parser::Util qw/read_config convert_date format_seconds/;

# TODO: 
# 1. Обновить версии модулей, исправить ошибки, обновить на проде

use constant {
    BODY => 0,
    HEADERS => 1,
};

my $cfg = read_config("$Bin/../etc/parser.conf");
my $log = Mojo::Log->new(path => "$Bin/../var/log/$cfg->{mojop_log}", level => $cfg->{mojop_log_level});
# my $log = Mojo::Log->new(path => 'app_mojop.log', level => $cfg->{mojop_log_level});

#use Data::Dumper;
#use Memory::Usage;
# version 3.0

## НАСТРОЙКИ и ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ ##
# TODO: добавить Getopt::Long
chomp(my $ru_domains_tmp_file = $ARGV[0]); # имя файла для чтения (для тестов разных кол-в)
$log->warn("File: $ru_domains_tmp_file not found!") and exit 0 unless -e $ru_domains_tmp_file;
my $start_time = $ARGV[1];
$start_time = 0 + $start_time;
# my $start_time = $ARGV[1] || time; # TODO: time+0
# $log->debug("start time: $start_time");
my @urls;

### COUNTS ###
my $cnt      = 0;    # служебный счетчик
chomp(my $total_line = `wc -l < $ru_domains_tmp_file`);

# ограничение на соединение внутри модуля AE::HTTP (по умолчанию 4 - здесь мы его меняем)
$AnyEvent::HTTP::MAX_PER_HOST = $cfg->{max_con};
$AnyEvent::HTTP::ACTIVE = $cfg->{max_con};
$AnyEvent::HTTP::TIMEOUT = 20;
$ENV{PERL_ANYEVENT_MAX_OUTSTANDING_DNS} = $cfg->{max_con};
$ENV{PERL_ANYEVENT_MAX_FORKS} = $cfg->{max_forks};
# $ENV{PERL_ANYEVENT_VERBOSE} = $cfg->{anyevent_log_level};
# $ENV{PERL_ANYEVENT_LOG} = 'log=file='.$cfg->{anyevent_log};

# my $warn_log = AnyEvent::Log::logger(warn => \my $warn); # export PERL_ANYEVENT_VERBOSE=5
# my $note_log = AnyEvent::Log::logger(note => \my $note); # export PERL_ANYEVENT_VERBOSE=6
# my $info_log = AnyEvent::Log::logger(info => \my $info); # export PERL_ANYEVENT_VERBOSE=7
# my $debug_log = AnyEvent::Log::logger(debug => \my $debug); # export PERL_ANYEVENT_VERBOSE=8

# коннект к БД
my $mongo         = MongoDB::MongoClient->new();
my $db            = $mongo->get_database("$cfg->{database}");
my $col_domains   = $db->get_collection('domains');
my $col_status    = $db->get_collection('status');
my $col_statistic = $db->get_collection('statistic');
my $col_domain_history = $db->get_collection('domain_history');

#################################
# AE::cv

my @reqs_time;

my $cv = AnyEvent->condvar;
my $req_time_begin = AnyEvent->time;

#$mu->record('before');
# читаем данные из файла
open my $fh, "<", $ru_domains_tmp_file or die "Can't open '$ru_domains_tmp_file' $!";
while (<$fh>) {
    my ($url, $reg, $reg_date, $pay_date, $end_date, $status) = split(/\s+/);
    next unless $url = lc($url);

    if ($status == 0) { # если статус = 0, удаляем такой домен из базы
        remove_url($url);
    }
    else {
        push( @urls, "http://www." . $url );    #  лучше запрашивать ресурс с www
        add_url( $url, $reg_date );
    }

    # начинаем выполенение только когда набили массив до максимума или пришли к концу файла
    if (@urls == $cfg->{max_con} or $total_line == $.) {
        # формируем очереди запросов и следим чтобы очередь всегда была заполнена (см. add_pool)
        add_pool() for 0 .. $#urls;
    }

}
close $fh;

$cv->recv;

for (0 .. $#reqs_time) {
    my $req_time = format_seconds($reqs_time[$_][1] - ($_ == 0 ? 0 : $reqs_time[$_ - 1][1]));
    my $url_host = $reqs_time[$_][0] =~ s{^http://www.}{}r; # r - Подстановка s///, не меняющая оригинал (perl 5.14+)

    stat_update('request_time');

    $log->debug("url: $url_host, request time: $req_time sec,");
    eval {
        $col_domains->update_one(
            { url => $url_host },
            {
                '$set' => {
                    request_time => ($req_time+0),
                    last_update   => $start_time
                },
            },
        );
    };

    $log->warn("DB1: $@") if $@;
}

# при завершении удаляем файл
END {
    # flush all date
    $mongo->fsync({async => 1});
    unlink($ru_domains_tmp_file) or $log->error("Cannot unlink $ru_domains_tmp_file!");
}

###################################################
#### SUBS ###
sub add_url {
    my $url      = shift;
    my $reg_date = convert_date(shift);

    $log->warn("[add_url] reg_date $reg_date") and return 0 unless int $reg_date;

    # обновляем статус по новым доменам если домена нет в нашей базе
    stat_update('newdomains') unless url_exists($url);

    eval {
        $col_domains->update_one(
            { url => $url },
            {
                '$set' => {
                    url      => $url,
                    reg_date => $reg_date
                  }

            },
            { upsert => 1 }
        );
    };
    $log->warn("DB1: $@") if $@;
}

sub remove_url {
    my $url = shift;

    # если домена нет - сразу возврат
    unless (url_exists($url)) {
        $log->debug("[remove_url] $url not exists in db");
        return 0;
    }

    # обновляем стату по удаленным если домен есть в базе
    stat_update('removed');
    eval { $col_domains->delete_one( { url => $url } ); };
    $log->warn("DB2: $@") if $@;
}
    

            
sub add_pool {
    return if $cnt >= $cfg->{max_con};
    return unless my $url = shift @urls;

#     return if $url =~/avtorradio.ru/g;

    my $body;
    ++$cnt;

    #print "Start ($cnt) $url\n";

    $cv->begin;
    http_get $url,
        cookie_jar  => {},
        persistent  => 0,
        keepalive   => 0,
        recurse     => 3,    # редиректы  - аналог LWP max_redirects
        timeout     => 20,
        headers     => {
            'user-agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
            referer      => 'http://hostx.ru/'
        },

        # только text/html страницы
        on_header => sub {
            $_[0]{"content-type"} =~ /^text\/html\s*(?:;|$)/;
        },

        # ограничение на размер ответа, если больше чем 1024*1024 - выходим 
        on_body => sub {
            $body .= $_[BODY];
            return length($body) <= 1024*1024;
        },

        sub {

            $log->debug("[http_get] url: $url, real_url: $_[HEADERS]{URL} $_[HEADERS]{Status} $_[HEADERS]{Reason}");
            # $debug_log->("[http_get] url: $url, real_url: $_[HEADERS]{URL} ") if $debug;
            # $log->debug("[http_get] url not exists: $url $header_url->{$url}") if not exists $header_url->{$url};
            # $log->debug("[http_get] status not exists for $url $_[HEADERS]{Status} $header_url->{$url}") if exists $header_url->{$url} and $header_url->{$url} ne $_[HEADERS]{Status} ;

            my $req_time_end = AnyEvent->time; # завершилось за $completed секунд/миллисекунд
            my $req_time  = format_seconds( $req_time_end - $req_time_begin );
            push @reqs_time, [$url, $req_time, $req_time_begin, $req_time_end];

            if ( $_[HEADERS]{Status} =~ /^2/ ) {
                $log->debug("OK: $url : $_[HEADERS]{'content-type'}"); # - хороший тон, но мало сайтов используют
                # $debug_log->("OK: $url : $_[HEADERS]{'content-type'}") if $debug; # - хороший тон, но мало сайтов используют
                parse_html( $body, $url );
            }
            else {
                error_get_page( $url, $_[HEADERS] );
            }
            --$cnt;
            $cv->end;
            add_pool();
    };

    return 1;
}

sub parse_html {
    my ($body, $url) = @_;

    my ($assistant, $cms, $call, $lang) = (0, 0, 0, 0);

    ## WEBCHAT ASSISTANT ##
    # TODO:
    # 1. оптимизировать, посмотреть \G, pos и Text::Haml
    # 2. добавить константы для $assistant, $cms, $call (use constants {assistant => { JIVOSITE => 1 }})
    if ( $body =~ m!//code\.jivosite\.com/script/widget/! ) {
        $assistant = 1;
        stat_update('jivosite');
    }
    elsif ( $body =~ m!web\.redhelper\.ru/service/! ) {
        $assistant = 2;
        stat_update('redhelper');
    }
    elsif ( $body =~ m!livetex\.ru/js/client\.js! ) {
        $assistant = 3;
        stat_update('livetex');
    }
    elsif ( $body =~ m!cdn\.livechatinc\.com! ) {
        $assistant = 4;
        stat_update('livechatinc');
    }
    elsif ( $body =~ m!static\.regidium\.com/widget! ) {
        $assistant = 5;
        stat_update('regidium');
    }
    elsif ( $body =~ m!//cdn\.krible\.com/loader! ) {
        $assistant = 6;
        stat_update('krible');
    }
    elsif ( $body =~ m!account\.marva\.ru! ) {
        $assistant = 7;
        stat_update('marva');
    }
    elsif ( $body =~ m!widget\.siteheart\.com! ) {
        $assistant = 8;
        stat_update('siteheart');
    }
    elsif ( $body =~ m!//code\.netroxsc\.ru! ) {
        $assistant = 9;
        stat_update('netroxsc');
    }
    elsif ( $body =~ m!chat\.chatra\.io/chatra\.js! ) {
        $assistant = 10;
        stat_update('chatra');
    }
    elsif ( $body =~ m!support\/support\.js\?h=! ) { # me-talk
        $assistant = 11;
        stat_update('metalk');
    }

    ## CMS ##
    if ( $body =~ m!/bitrix/templates/! ) {
        $cms = 1;
        stat_update('bitrix');
    }
    elsif ( $body =~ m!/sites/all/! ) {
        $cms = 2;
        stat_update('drupal');
    }
    elsif ( $body =~ m!wp-content/themes/! ) {
        $cms = 3;
        stat_update('wordpress');
    }
    elsif ( $body =~ m!media/system/! ) {
        $cms = 4;
        stat_update('joomla');
    }
    elsif ( $body =~ m!assets/templates/! ) {
        $cms = 5;
        stat_update('modx');
    }

    ## CALLBACK SYSTEM
    if ( $body =~ m!//callbackhunter\.com/widget/tracker\.js! ) {
        $call = 1;
        stat_update('callbackhunter');
    } elsif($body =~ m!//perezvoni\.com/files/widgets/!) {
        $call = 2;
        stat_update('perezvoni');
    }

    # 1 - русский язык - не работает по факту
    #$lang = 1 if $body =~ m![а-я]!;
    my $url_host = $url =~ s{^http://www.}{}r; # r - Подстановка s///, не меняющая оригинал (perl 5.14+)

    $log->debug("[parse_html] $url $url_host (call=$call, cms=$cms, assistant=$assistant)") if $assistant or $cms or $call;
    # my $url_host = $url;
    # $url_host =~ s/^http:\/\/www.//g;
    eval {
        $col_domains->update_one(
            { url => $url_host },
            {
                '$set' => {
                    url           => $url_host,
                    cms           => $cms,
                    assistant     => $assistant,
                    call          => $call,
                    lang          => $lang,
                    status        => 200,
                    error_message => '',
                    last_update   => $start_time
                },
                # '$push' => {
                #     history => {
                #         ts        => $start_time,
                #         cms       => $cms,
                #         assistant => $assistant,
                #         call      => $call,
                #     },
                # },
            }
        );
    };
    $log->warn("DB4: $@") if $@;
    eval {
        $col_domain_history->update_one(
            { url => $url_host },
            {
                '$push' => {
                    history => {
                        ts        => $start_time,
                        cms       => $cms,
                        assistant => $assistant,
                        call      => $call,
                    },
                },
            },
            { upsert => 1 },
        );
    };
    $log->warn("[domain_history] push error: $@") if $@;
}

# Обновление статы вынесем в отдельную функцию
sub stat_update {
    my $assist = shift;
    eval {
        $col_statistic->update_one(
            { ts => $start_time },
            {
                '$inc' => { $assist => 1 }
            },
            { upsert => 1 }
        );
    };
    $log->warn("DB3: $@") if $@;
}

sub error_get_page {
    my ( $url, $err ) = @_;
    my $url_host = $url =~ s{^http://www.}{}r; # r - Подстановка s///, не меняющая оригинал (perl 5.14+)
    # TODO: в лог пишутся иероглифы для Reason, поправить вывод в utf8
    $log->debug("BAD: $url_host @$err{qw/Status Reason/} $err->{'content-type'} ");
    stat_update('errors');
    eval {
        $col_domains->update_one(
            { url => $url_host },
            {
                '$set' => {
                    url           => $url_host,
                    status        => $err->{Status},
                    error_message => $err->{Reason},
                    last_update   => $start_time
                }
            }
        );
    };
    $log->warn("DB4: $@") if $@;
}

sub url_exists {
   my $url = shift;
   my $string  = eval { $col_domains->find_one({url => $url}) };

   $log->warn("[url_exists] can't get url $url from parser.domains: $@") and return undef if $@;

   return $string && $string->{url};
}
