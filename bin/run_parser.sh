#!/bin/sh

export HOME="/home/titan"
export PATH="$HOME/.anyenv/envs/plenv/bin:$HOME/.anyenv/envs/plenv/versions/5.20.3/bin:$PATH"
# export PATH="$HOME/.anyenv/envs/plenv/bin:$PATH"

# eval "$(plenv init -)"
# exec $SHELL -l
# eval "$(plenv rehash)"

cd $HOME/titan/bin

exec carton exec -- perl parser_worker.pl
# exec carton exec -- "$1"
