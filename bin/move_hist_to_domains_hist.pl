#!/usr/bin/env perl -w
use warnings;
use strict;
use feature 'say';
use DDP;
use MongoDB;
use constant DEBUG => 1;

# коннект к БД
my $mongo       = MongoDB::MongoClient->new(query_timeout => 180_000_000);
my $db          = $mongo->get_database('parser');
my $col_domains = $db->get_collection('domains');
my $col_domain_history = $db->get_collection('domain_history');

# # выборка документов у которых есть history
# my $hist = $col_domains->find( { history => { '$exists' => 1 } } );
#
# my $hist_count = $hist->count();
#
# # Переносим массив history в коллекцию domains_history
# my $cnt = 0;
# # TODO: реализовать с помощью insert_many - в цикле создавать массив n (например, 1000), затем при заполнении массива, записывать
# while ( my $doc = $hist->next ) {
#     say ((++$cnt)."/$hist_count ".$doc->{url}) if DEBUG;
#
#     my $result = $col_domain_history->insert_one({url => $doc->{url}, history => $doc->{history}});
#
#     if ($result->acknowledged) {
#         $col_domains->update(
#             { url => $doc->{url} },
#             {
#                 '$unset' => { history => $doc->{history} }
#             },
#         );
#     } else {
#         warn "Error insert history for $doc->{url} to domain_history";
#     }
#
# }

use Safe::Isa;
use Try::Tiny;
 
my $bulk_domain_history = $col_domain_history->initialize_ordered_bulk_op;
my $bulk_domains = $col_domains->initialize_ordered_bulk_op;

# # выборка документов у которых есть history
my $hist = $col_domains->find( { history => { '$exists' => 1 } } );
 
my $hist_count = $col_domains->count( { history => { '$exists' => 1 } } );
say "count domains collection with history field: $hist_count" if DEBUG;
my $x = 5_000;
my $cnt=0;

while ( my $doc = $hist->next ) {

    $bulk_domain_history->insert_one({url => $doc->{url}, history => $doc->{history}});
    $bulk_domains->find({ _id => $doc->{_id} })->update(
        {
            '$unset' => { history => $doc->{history} }
        },
    );

    ++$cnt;

    if ($cnt % $x == 0) {
	    say ("$cnt/$hist_count ".$doc->{url}) if DEBUG;

        my $result = try {
            $bulk_domain_history->execute;
            $bulk_domains->execute;
            $bulk_domain_history = $col_domain_history->initialize_ordered_bulk_op;
            $bulk_domains = $col_domains->initialize_ordered_bulk_op;
        }
        catch {
            if ( $_->$_isa("MongoDB::WriteConcernError") ) {
                warn "Write concern failed";
            }
            else {
                die $_;
            }
        };

    }

    # my $result = $col_domain_history->insert_one({url => $doc->{url}, history => $doc->{history}});
    #
    # if ($result->acknowledged) {
    #     $col_domains->update(
    #         { url => $doc->{url} },
    #         {
    #             '$unset' => { history => $doc->{history} }
    #         },
    #     );
    # } else {
    #     warn "Error insert history for $doc->{url} to domain_history";
    # }

}

if ($hist_count) {
    my $result = try {
        $bulk_domain_history->execute;
        $bulk_domains->execute;
    }
    catch {
        if ( $_->$_isa("MongoDB::WriteConcernError") ) {
            warn "Write concern failed";
        }
        else {
            die $_;
        }
    };
}

# $bulk->insert_one( $doc );
# $bulk->find( $query )->upsert->replace_one( $doc )
# $bulk->find( $query )->update( $modification )
#  
# my $result = try {
#     $bulk->execute;
# }
# catch {
#     if ( $_->$isa("MongoDB::WriteConcernError") ) {
#         warn "Write concern failed";
#     }
#     else {
#         die $_;
#     }
# };


# my $db_test = $mongo->get_database('test_parser');
# my $col_test = $db_test->get_collection('test_domains');
#
# # my $cnt=0;
# # for ('a'..'z') {
# #     ++$cnt;
# #     my $result = $col_test->insert_one({$_ => $cnt, test => "test$_", name => "vasya$_", nums => [{a => 1, b=>2, c=>3}, {a => 2, b=>3, d=>4}]});
# #     # if ( $result->acknowledged ) {
# #     #     # update
# #     #     }
# # }
#
# my $cursor = $col_test->find({test => {'$exists' => 1}});
# while (my $doc = $cursor->next) {
#     $col_test->update({
#             name => $doc->{name}
#         },
#         {
#             '$unset' => { nums => $doc->{nums} },
#         },
#         # {multi => 1}
#     )
# }

# > var bulkInsert = db.target.initializeUnorderedBulkOp()
# > var bulkRemove = db.source.initializeUnorderedBulkOp()
# > var x = 10000
# > var counter = 0
# > var date = new Date()
# > date.setMonth(date.getMonth() -1)
# > db.source.find({"yourDateField":{$lt: date}}).forEach(
#     function(doc){
#       bulkInsert.insert(doc);
#       bulkRemove.find({_id:doc._id}).removeOne();
#       counter ++
#       if( counter % x == 0){
#         bulkInsert.execute()
#         bulkRemove.execute()
#         bulkInsert = db.target.initializeUnorderedBulkOp()
#         bulkRemove = db.source.initializeUnorderedBulkOp()
#       }
#     }
#   )
# > bulkInsert.execute()
# > bulkRemove.execute()
