#!/usr/bin/perl -w

use common::sense;
use MongoDB;
use IO::File;
use Mojo::Log;
use POSIX ":sys_wait_h";
use FindBin qw($Bin);
use DDP;

use FindBin qw/$Bin/;
use lib "$Bin/../lib";
# TODO: Parser::Util -> Regidium::Parser::Util
use Titan::Parser::Util qw/read_config/;

# ver 3.0
################ GLOBALS VARS ##################################
my $cfg = read_config("$Bin/../etc/parser.conf");
my $log = Mojo::Log->new(path => "$Bin/../var/log/$cfg->{worker_log}", level => $cfg->{worker_log_level});

my $start_time     = time;
$start_time = $start_time+0;

# уборка зомби
$SIG{CHLD} = sub { while(waitpid(-1,WNOHANG)>0) {}};
####### WGET #############
unless ($cfg->{debug}) {
    system(
        "wget --no-check-certificate --output-document=ru_domains.gz 'https://www.nic.ru/downloads/ru_domains.gz?login=3193/NIC-REG&password=pro222A'"
    );
    system("gzip -fd  $cfg->{file}.gz");
}
####### OTHER GLOBAL VARS #############################
# для подсчета кол-ва строк в файле делаем системный вызов
chomp(my $count_all = `wc -l < $cfg->{file}`);
my $cnt      = 0;    # служебный счетчик
my $cur_line = 0;    # текущая позиция большого файла
my $file_it = 1; # итератор файлов для подсчета их кол-ва
# коннект к БД
my $mongo         = MongoDB::MongoClient->new();
my $db            = $mongo->get_database("$cfg->{database}");# db name to file
my $col_domains   = $db->get_collection('domains');
my $col_status    = $db->get_collection('status');
my $col_statistic = $db->get_collection('statistic');

#######################################################
# обновляем статистику по запуску лаунчера
eval {
    $col_statistic->update_one(
        { ts => $start_time },
        {
            '$set' => {
                       ts => $start_time,
                       total_domains => $count_all,
                       }
        },
        { upsert => 1 }
    );
};
$log->warn("DB1: $@") if $@;

eval {
    $col_status->update_one(
        { status => 'curent' },
        {
            '$set' => {
                status          => 'curent',
                start_time      => time,
                count_domains   => $count_all,
                current_domains => 0,
                working         => 1,
            }
        },
        { upsert => 1 }
    );
};
$log->warn("DB2: $@") if $@;
# unlink <*.tmp> or $log->error("Can't unlink tmp files: $!");# удаляем все временные файлы перед запуском обработки
unlink <*.tmp> or die "Can't unlink tmp files: $!" if <*.tmp>;# удаляем все временные файлы перед запуском обработки

#### MAIN ####

# Алгоритм работы такой:
# генерим временный файл в N доменов, и отдаем скрипту mojop.pl filename.tmp start_time
# ждем отработки скрипта - удаляем файл, генерим следующий файл и снова запускам скрипт
open my $ru_domains_fh, '<', $cfg->{file} or die $log->error("Open file error: $!");
my $fh = new IO::Handle;

while (<$ru_domains_fh>) {
    ++$cnt;
    ++$cur_line; # TODO: $.

    open $fh, '>>', "$cfg->{file}.$file_it.tmp" or die $! unless $fh->opened();

    # unless ( $fh->opened() ) {
    #     open $fh, '>>', "$cfg->{file}.$file_it.tmp" or die $!;
    # }
    print $fh $_;
    ### если достигли максимального кол-ва строк в файле (временном), либо это последняя строка файла основного
    if ( $cnt == $cfg->{temp_file_line} or $count_all == $cur_line ) {
        close $fh or die $!;
        $cnt = 0;
        my $child = fork();
        # форкаемся и отдаем на выполнение скрипт
        if ($child == 0) {
            # $log->warn("--- exec perl mojop.pl $cfg->{file}.$file_it.tmp $start_time"); # TODO: log->debug
            # запускаем скрипт на выполнение
            exec("perl parser_mojop.pl $cfg->{file}.$file_it.tmp $start_time");
        }# в родителе
        else {
            # увеличиваем итератор файлов
            $file_it++;
            # обновляем статистику 
            update_status($cur_line);
            check_queue($child);# проверяем очередь и если больше - спим
        }

    }
}

close($ru_domains_fh);

#################################################
# завершение работы отражаем в статусе
eval {
    $col_status->update_one(
        { status => 'curent' },
        {
            '$set' => {
                status    => 'curent',
                stop_time => time,
                working   => 0,
            }
        },
        { upsert => 1 }
    );
};
$log->warn("DB3: $@") if $@;
### SUBS ###
sub update_status {
    my $done =
      $cur_line /
      $count_all * 100
      ; # считаем процент выполнения от общего кол-ва доменов
    eval {
        $col_status->update_one(    # обвноляем статус в БД
            { status => 'curent' },
            {
                '$set' => {
                    status          => 'curent',
                    start_time      => $start_time,
                    count_domains   => $count_all,
                    current_domains => $cur_line,
                    working         => 1,
                    done            => $done
                }
            },
            { upsert => 1 }
        );
    };
    $log->warn("DB4: $@") if $@;
}

sub check_queue {
    # пока файлов больше положенного спим и проверяем каждые 60 сек
    while (check_file()) {
        sleep 60;
    }
    
}

sub check_file {
    my $current_dir = '.';

    # TODO: нормально обработать ошибки
    opendir my $dh, $current_dir or $log->warn("Can't open dir $current_dir $!");
    my $cnt_tmp_domain_files = () = grep /\d+\.tmp$/, readdir $dh; # исключаем лишнее из папки
    close $dh;

    $log->debug("Tmp domain file count: $cnt_tmp_domain_files") if $cfg->{debug};

    return $cnt_tmp_domain_files >= $cfg->{fork}; # если файлов в каталоге больше чем очередь - true
}

