#!/usr/bin/env perl
use strict;
use warnings;
use feature 'say';
use DDP {color=>{string=>'black'}};

use List::MoreUtils qw/natatime/;
use MongoDB;

my $mongo       = MongoDB::MongoClient->new();
my $db          = $mongo->get_database('parser');
my $col_domains = $db->get_collection('domains');
my $col_domain_history = $db->get_collection('domain_history');

my %query = (
    status => 200,
    # url => "100koles59.ru",
);

my $domains_count = $col_domains->count( \%query );
# my $domains_count = $col_domains->find( \%query )->count;
# my $domains_count = $col_domains->find( {status => 200} )->count;
say "domains count: $domains_count";

my $domain = $col_domains->find_one(\%query);
my $domain_history = $col_domain_history->find_one({url => $domain->{url}});
p $domain;
p $domain_history;

# my $page_id = 0;
#
# my $cursor = $col_domains
#     ->find( \%query )
#     ->limit(50)
#     ->skip( ( ($page_id || 1) - 1 ) * 50 )
#     ->sort({url => 1});
#
# p $cursor;
#
# my @arr = qw/a 1 b 2 c 3/;
#
# my $find_str = '';
# my $it = natatime 2, @arr;
# while (my @vals = $it->()) {
#     $find_str .= join(' : ', @vals).', ';
# }
# p @arrs;
# say $find_str;
# # say sprintf('db.domains.find(%s)', );
