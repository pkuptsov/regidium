#!/usr/bin/perl -w

use common::sense;

use DDP;

use FindBin qw/$Bin/;
use lib "$Bin/lib";
use Parser::Util qw/read_config convert_date format_seconds/;

my $date = 'aa.06.2017';
my $reg_date = eval { convert_date($date) };
say "can't convert date $date: $@" and exit 0 if $@;

# $log->warn("[add_url] reg_date $reg_date") and return 0 unless int $reg_date;
