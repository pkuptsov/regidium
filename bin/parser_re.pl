#!/usr/bin/perl -w

use warnings;
use strict;
use DDP;

my $cnt = 0;

chomp(my $file = $ARGV[0]);

open(FIL,"<",$file) || die "$!";
open(NEW,">>",'ru_domains_cut');
while(<FIL>) {
    $cnt++;
    if($cnt >= 224000) {
        print NEW $_;
    }
}
close(FIL);
close(NEW);
