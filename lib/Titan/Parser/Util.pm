package Titan::Parser::Util;

use strict;
use warnings;

use feature 'say';

our @EXPORT_OK = qw/read_config convert_date format_time format_seconds/;
use base qw/Exporter/;

use constant {
    CONFIG => 0,
};

use DDP;
use Scalar::Util qw/dualvar/;
use Time::Local;
use Carp;

# TODO: добавить тест
sub read_config {
    my $config;

    open my $conf_fh, '<', $_[CONFIG] or die "Can't open config file $_[CONFIG]: $!";
    while (<$conf_fh>) {
        chomp;
        next if /^#/;
        $config->{$+{name}} = $+{value} if /(?<name>.+)\s+=\s+(?<value>.+)/g;
    }
    close $conf_fh;

    return $config;
}

# sub convert_date {
#     my $time = shift;
#     if ( $time =~ m/(?<mday>[0-9]{2})\.(?<mon>[0-9]{2})\.(?<year>[0-9]{4})/ ) {
#         return timelocal( 12, 0, 0, $+{mday}, $+{mon} - 1, $+{year} );
#     }
#     else {
#         croak "[convert_date] ERROR convert date to local time: $time, time should be in format 'dd.dd.dddd'";
#     }
# }

sub convert_date {
    my $time = shift;
    if ( $time =~ m/(?<mday>[0-9]{2})\.(?<mon>[0-9]{2})\.(?<year>[0-9]{4})/ ) {
        my $timelocal = eval { timelocal( 12, 0, 0, $+{mday}, $+{mon} - 1, $+{year} ) };
        return $@ ? dualvar(0, "[covert_date] ERROR $@") : $timelocal;
    }
    else {
        return dualvar 0, "[convert_date] ERROR convert date to local time: $time, time should be in format 'dd.dd.dddd'";
    }
}

sub format_time {
    my ( $microsec, $seconds ) = modf(shift);

    my ( $sec, $min, $hour ) = localtime($seconds);

    return sprintf "%02d:%02d:%02d.%04d", $hour, $min, $sec, int( $microsec * 10000 );
}

sub format_seconds {
    sprintf "%.4f", $_[0];
}

1;
